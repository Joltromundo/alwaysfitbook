<h1>Documentação da API</h1>
<p>Esta documentação explica os endpoints disponíveis na API e suas respostas.</p>

<h2>Como Executar o Projeto</h2>
<p>Para executar o projeto, siga os seguintes passos:</p>
<ol>
    <li>Abra um terminal na raiz do projeto.</li>
    <li>Execute o seguinte comando para iniciar os serviços Docker:</li>
</ol>
<pre>
docker-compose up --build
</pre>
<p>Isso irá compilar e iniciar os serviços necessários para a aplicação.</p>

<h2>Autenticação</h2>
<p>Antes de acessar os endpoints protegidos, você precisa obter um token JWT fazendo login.</p>

<h3>POST /api/auth</h3>
<p>Endpoint para autenticar um usuário e receber um token JWT.</p>
<p>Resposta:</p>
<pre>
{
    "token": "seu-token-jwt"
}
</pre>

<h3>POST /api/register</h3>
<p>Endpoint para registrar um novo usuário.</p>
<p>Corpo da Requisição:</p>
<pre>
{
    "email": "seu@email.com",
    "password": "sua-senha"
}
</pre>
<p>Resposta:</p>
<pre>
{
    "message": "Usuário registrado com sucesso"
}
</pre>

<h2>Endpoints Protegidos</h2>
<p>Esses endpoints exigem um token JWT válido obtido no processo de autenticação.</p>
<p>Basta adicionar o token ao no Header Authorization</p>

<h3>GET /api/albums</h3>
<p>Obter uma lista de álbuns ou recuperar um álbum específico por ID.</p>
<p>Resposta:</p>
<pre>
{
    "data": {
        // Dados do álbum
    }
}
</pre>

<h3>GET /api/albums</h3>
<p>Obter uma lista de álbuns ou recuperar um álbum específico por ID (adicionando o parameter ?id={id}).</p>
<p>Resposta:</p>
<pre>
{
    "data": {
        // Dados do álbum
    }
}
</pre>

<h3>POST /api/albums</h3>
<p>Criar um novo álbum.</p>
<p>Corpo da Requisição:</p>
<pre>
{
    "title": "Título do Álbum"
}
</pre>
<p>Resposta:</p>
<pre>
{
    "message": "Álbum criado com sucesso"
}
</pre>

<h3>PUT /api/albums?id={id}</h3>
<p>Atualizar um álbum existente por ID.</p>
<p>Corpo da Requisição:</p>
<pre>
{
    "title": "Título do Álbum Atualizado"
}
</pre>
<p>Resposta:</p>
<pre>
{
    "message": "Álbum atualizado com sucesso"
}
</pre>

<h3>DELETE /api/albums?id={id}</h3>
<p>Excluir um álbum por ID.</p>
<p>Resposta:</p>
<pre>
{
    "message": "Álbum excluído com sucesso"
}
</pre>

<h3>GET /api/photos</h3>
<p>Obter uma lista de fotos ou recuperar uma foto específica por ID (adicionando o parameter ?id={id}).</p>
<p>Resposta:</p>
<pre>
{
    "data": {
        // Dados da foto
    }
}
</pre>

<h3>POST /api/photos</h3>
<p>Criar uma nova foto.</p>
<p>Corpo da Requisição:</p>
<pre>
{
    "image_url": "URL da imagem",
    "title": "Título da Foto"
}
</pre>
<p>Resposta:</p>
<pre>
{
    "message": "Foto criada com sucesso"
}
</pre>

<h3>PUT /api/photos?id={id}</h3>
<p>Atualizar uma foto existente por ID.</p>
<p>Corpo da Requisição:</p>
<pre>
{
    "title": "Título da Foto Atualizado"
}
</pre>
<p>Resposta:</p>
<pre>
{
    "message": "Foto atualizada com sucesso"
}
</pre>

<h3>DELETE /api/photos?id={id}</h3>
<p>Excluir uma foto por ID.</p>
<p>Resposta:</p>
<pre>
{
    "message": "Foto excluída com sucesso"
}
</pre>
