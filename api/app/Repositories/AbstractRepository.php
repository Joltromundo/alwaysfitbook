<?php

namespace app\Repositories;
use PDO;

abstract class AbstractRepository {
    protected $connection;
    protected $model;

    public function __construct($model) {
        $this->connection = new PDO('mysql:host=mysql;dbname=alwaysfitbook', 'alwaysfitbook_user', '123');
        $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        $this->model = $model;
    }

    public function getAll() {
        $query = "SELECT * FROM {$this->getTableName()}";

        $stmt = $this->connection->query($query);
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getById($id) {
        $tableName = $this->getTableName();
        $query = "SELECT * FROM $tableName WHERE id = :id";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function create($data) {
        $columns = implode(', ', array_keys($data));
        $placeholders = ':' . implode(', :', array_keys($data));
        
        $query = "INSERT INTO {$this->getTableName()} ($columns) VALUES ($placeholders)";

        $stmt = $this->connection->prepare($query);

        foreach ($data as $key => $value) {
            $stmt->bindValue(":$key", $value);
        }

        return $stmt->execute();
    }

    public function update($id, $data) {
        $updateValues = '';
        foreach ($data as $key => $value) {
            $updateValues .= "$key = :$key, ";
        }
        $updateValues = rtrim($updateValues, ', ');

        $query = "UPDATE {$this->getTableName()} SET $updateValues WHERE id = :id";
        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);

        foreach ($data as $key => $value) {
            $stmt->bindValue(":$key", $value);
        }

        return $stmt->execute();
    }

    public function delete($id) {
        $query = "DELETE FROM {$this->getTableName()} WHERE id = :id";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':id', $id, PDO::PARAM_INT);
        return $stmt->execute();
    }
}