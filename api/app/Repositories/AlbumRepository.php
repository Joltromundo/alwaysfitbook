<?php

namespace app\Repositories;
use app\Models\Album;
use PDO;

class AlbumRepository extends AbstractRepository {
    public function __construct() {
        parent::__construct(new Album());
    }

    protected function getTableName() {
        return "albums";
    }

    public function getAllByUser($user_id) {
        $tableName = $this->getTableName();
        $query = "SELECT * FROM $tableName WHERE user_id = :user_id";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}