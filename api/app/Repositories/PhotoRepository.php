<?php

namespace app\Repositories;
use app\Models\Photo;
use PDO;

class PhotoRepository extends AbstractRepository {
    public function __construct() {
        parent::__construct(new Photo());
    }

    protected function getTableName() {
        return "photos";
    }

    public function getAllByUser($user_id) {
        $tableName = $this->getTableName();
        $query = "SELECT * FROM $tableName WHERE user_id = :user_id";

        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}