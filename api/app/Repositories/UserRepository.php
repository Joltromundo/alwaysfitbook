<?php

namespace app\Repositories;
use app\Models\User;
use PDO;

class UserRepository extends AbstractRepository {
    public function __construct() {
        parent::__construct(new User());
    }

    protected function getTableName() {
        return "users";
    }

    public function findByEmail($email) {
        $query = "SELECT * FROM {$this->getTableName()} WHERE email = :email LIMIT 1";
        $stmt = $this->connection->prepare($query);
        $stmt->bindParam(':email', $email);
        $stmt->execute();

        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}