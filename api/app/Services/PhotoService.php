<?php

namespace app\Services;

use app\Models\Photo;
use app\Repositories\PhotoRepository;

class PhotoService {
    private $photoRepository;

    public function __construct() {
        $this->photoRepository = new PhotoRepository();
    }

    public function getAllPhotos($user_id) {
        return $this->photoRepository->getAllByUser($user_id);
    }

    public function getPhotoById($id) {
        return $this->photoRepository->getById($id);
    }

    public function createPhoto($title, $imageUrl, $album_id, $user_id) {
        $photo = new Photo();
        $photo->setTitle($title);
        $photo->setImageUrl($imageUrl);
        $photo->setAlbumId($album_id);
        $photo->setUserId($user_id);

        $photoData = array(
            'title' => $photo->getTitle(),
            'image_url' => $photo->getImageUrl(),
            'album_id' => $photo->getAlbumId(),
            'user_id' => $photo->getUserId(),
        );

        return $this->photoRepository->create($photoData);
    }

    public function updatePhoto($id, $title, $imageUrl, $album_id, $user_id) {
        $photo = new Photo();
        $photo->setTitle($title);
        $photo->setImageUrl($imageUrl);
        $photo->setAlbumId($album_id);
        $photo->setUserId($user_id);

        $photoData = array(
            'title' => $photo->getTitle(),
            'image_url' => $photo->getImageUrl(),
            'album_id' => $photo->getAlbumId(),
            'user_id' => $photo->getUserId(),
        );

        return $this->photoRepository->update($id, $photoData);
    }

    public function deletePhoto($id) {
        return $this->photoRepository->delete($id);
    }
}