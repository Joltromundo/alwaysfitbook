<?php
namespace app\Services;

use app\Repositories\UserRepository;

class AuthService {
    protected $userRepository;

    public function __construct() {
        $this->userRepository = new UserRepository();
    }

    public function authenticate($email, $password) {
        $user = $this->userRepository->findByEmail($email);

        if ($user && password_verify($password, $user['password'])) {
            return $user;
        }

        return null;
    }
}