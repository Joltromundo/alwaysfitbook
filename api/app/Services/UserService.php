<?php

namespace app\Services;

use app\Models\User;
use app\Repositories\UserRepository;

class UserService {
    private $userRepository;

    public function __construct() {
        $this->userRepository = new UserRepository();
    }

    public function getAllUsers() {
        return $this->userRepository->getAll();
    }

    public function getUserById($id) {
        return $this->userRepository->getById($id);
    }

    public function createUser($name, $email, $password) {
        $user = new User();
        $user->setName($name);
        $user->setEmail($email);
        $user->setPassword(password_hash($password, PASSWORD_DEFAULT));

        $userData = array(
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword()
        );

        return $this->userRepository->create($userData);
    }

    public function updateUser($id, $name, $email, $password) {
        $user = new User();
        $user->setId($id);
        $user->setName($name);
        $user->setEmail($email);
        $user->setPassword(password_hash($password, PASSWORD_DEFAULT));

        $userData = array(
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => $user->getPassword()
        );

        return $this->userRepository->update($id, $userData);
    }

    public function deleteUser($id) {
        return $this->userRepository->delete($id);
    }
}