<?php

namespace app\Services;

use app\Models\Album;
use app\Repositories\AlbumRepository;

class AlbumService {
    private $albumRepository;

    public function __construct() {
        $this->albumRepository = new AlbumRepository();
    }

    public function getAllAlbums($user_id) {
        return $this->albumRepository->getAllByUser($user_id);
    }

    public function getAlbumById($id) {
        return $this->albumRepository->getById($id);
    }

    public function createAlbum($title, $user_id) {
        $album = new Album();
        $album->setTitle($title);
        $album->setUserId($user_id);

        $albumData = array(
            'title' => $album->getTitle(),
            'user_id' => $album->getUserId(),
        );

        return $this->albumRepository->create($albumData);
    }

    public function updateAlbum($id, $title, $user_id) {
        $album = new Album();
        $album->setId($id);
        $album->setTitle($title);
        $album->setUserId($user_id);

        $albumData = array(
            'title' => $album->getTitle(),
            'user_id' => $album->getUserId(),
        );

        return $this->albumRepository->update($id, $albumData);
    }

    public function deleteAlbum($id) {
        return $this->albumRepository->delete($id);
    }
}