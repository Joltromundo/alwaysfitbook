<?php
    namespace App\Models;
    
    class Photo {
        private $id;
        private $title;
        private $imageUrl;
        private $createdDate;
        private $user_id;
        private $album_id;
    
        public function getId() {
            return $this->id;
        }

        public function setId($id) {
            $this->id = $id;
        }

        public function getTitle() {
            return $this->title;
        }

        public function setTitle($title) {
            $this->title = $title;
        }

        public function getImageUrl() {
            return $this->imageUrl;
        }

        public function setImageUrl($imageUrl) {
            $this->imageUrl = $imageUrl;
        }

        public function getCreatedDate() {
            return $this->createdDate;
        }

        public function setCreatedDate($createdDate) {
            $this->createdDate = $createdDate;
        }

        public function getUserId() {
            return $this->user_id;
        }

        public function setUserId($user_id) {
            $this->user_id = $user_id;
        }

        public function getAlbumId() {
            return $this->album_id;
        }

        public function setAlbumId($album_id) {
            $this->album_id = $album_id;
        }
    }
?>