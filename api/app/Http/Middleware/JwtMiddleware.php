<?php

namespace app\Http\Middleware;

class JwtMiddleware {
    public function handle($request, $next) {
        $headers = getallheaders();
        
        $token = isset($headers['Authorization']) ? $headers['Authorization'] : '';

        if (!$token) {
            http_response_code(401);
            echo json_encode(array("message" => "Token JWT não fornecido."));
            return;
        }

        if ($this->verificarToken($token)) {
            return $next($request);
        } else {
            http_response_code(401);
            echo json_encode(array("message" => "Token JWT inválido."));
            return;
        }
    }

    private function verificarToken($token) {
        $chaveSecreta = 'd2YYDHyL9P3YGJ2q9jKVeySMhwpXj';

        $partes = explode('.', $token);
        if (count($partes) !== 3) {
            return false;
        }

        $payload = base64_decode($partes[1]);
        $payload = json_decode($payload, true);

        $assinaturaCalculada = hash_hmac('sha256', $partes[0] . '.' . $partes[1], $chaveSecreta, true);
        $assinaturaCalculada = base64_encode($assinaturaCalculada);

        if ($assinaturaCalculada !== $partes[2]) {
            return false;
        }

        $agora = time();
        if (isset($payload['exp']) && $payload['exp'] < $agora) {
            return false;
        }

        return true;
    }
}
