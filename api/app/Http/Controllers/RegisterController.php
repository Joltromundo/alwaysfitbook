<?php
namespace app\Http\Controllers;

use app\Services\UserService;
use app\Http\Requests\UserRequest;

class RegisterController {
    private $userService;

    public function __construct(UserService $userService) {
        $this->userService = $userService;
    }

    public function store(){
        $validation = UserRequest::storeValidate($_POST);

        if (empty($validation)) {
            $this->userService->createUser($_POST['name'], $_POST['email'], $_POST['password']);

            echo json_encode(['message' => 'Registro realizado com sucesso']);
        } else {
            echo json_encode(['error' => $validation]);
        }
    }
}