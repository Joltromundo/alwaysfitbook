<?php
namespace app\Http\Controllers;

use app\Services\PhotoService;
use app\Http\Requests\PhotoRequest;

class PhotoController {
    private $photoService;

    public function __construct(PhotoService $photoService) {
        $this->photoService = $photoService;
    }

    public function index(){
        $headers = getallheaders();
        $token = substr($headers['Authorization'], 7);

        $partes = explode('.', $token);
        if (count($partes) === 3) {
            $payload = base64_decode($partes[1]);
            $payload = json_decode($payload, true);

            $userId = $payload['user_id'];
        } else {
            echo "Token JWT inválido!";
        }

        $photos = $this->photoService->getAllPhotos($userId);

        echo json_encode($photos);
    }

    public function store(){
        $validation = PhotoRequest::storeValidate($_POST);

        if (empty($validation)) {
            $headers = getallheaders();
            $token = substr($headers['Authorization'], 7);

            $partes = explode('.', $token);
            if (count($partes) === 3) {
                $payload = base64_decode($partes[1]);
                $payload = json_decode($payload, true);

                $userId = $payload['user_id'];
            } else {
                echo "Token JWT inválido!";
            }

            $this->photoService->createPhoto($_POST['title'], $_POST['image_url'], $_POST['album_id'] , $userId);

            echo json_encode(['message' => 'Registro realizado com sucesso']);
        } else {
            echo json_encode(['error' => $validation]);
        }
    }

    public function edit(){
        $album = $this->photoService->getPhotoById($_GET['id']);

        echo json_encode($album);
    }

    public function update(){
        $validation = PhotoRequest::storeValidate($_GET);

        if (empty($validation)) {
            $headers = getallheaders();
            $token = substr($headers['Authorization'], 7);

            $partes = explode('.', $token);
            if (count($partes) === 3) {
                $payload = base64_decode($partes[1]);
                $payload = json_decode($payload, true);

                $userId = $payload['user_id'];
            } else {
                echo "Token JWT inválido!";
            }

            $this->photoService->updatePhoto($_GET['id'], $_GET['title'], $_GET['image_url'], $_GET['album_id'] , $userId);

            echo json_encode(['message' => 'Registro atualizado com sucesso']);
        } else {
            echo json_encode(['error' => $validation]);
        }
    }

    public function delete(){
        $this->photoService->deletePhoto($_GET['id']);

        echo json_encode(['message' => 'Registro deletado com sucesso']);
    }
}