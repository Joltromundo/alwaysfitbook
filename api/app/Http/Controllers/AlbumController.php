<?php
namespace app\Http\Controllers;

use app\Services\AlbumService;
use app\Http\Requests\AlbumRequest;

class AlbumController {
    private $albumService;

    public function __construct(AlbumService $albumService) {
        $this->albumService = $albumService;
    }

    public function index(){
        $headers = getallheaders();
        $token = substr($headers['Authorization'], 7);

        $partes = explode('.', $token);
        if (count($partes) === 3) {
            $payload = base64_decode($partes[1]);
            $payload = json_decode($payload, true);

            $userId = $payload['user_id'];
        } else {
            echo "Token JWT inválido!";
        }

        $albums = $this->albumService->getAllAlbums($userId);

        echo json_encode($albums);
    }

    public function store(){
        $validation = AlbumRequest::storeValidate($_POST);

        if (empty($validation)) {
            $headers = getallheaders();
            $token = substr($headers['Authorization'], 7);

            $partes = explode('.', $token);
            if (count($partes) === 3) {
                $payload = base64_decode($partes[1]);
                $payload = json_decode($payload, true);

                $userId = $payload['user_id'];
            } else {
                echo "Token JWT inválido!";
            }

            $this->albumService->createAlbum($_POST['title'], $userId);

            echo json_encode(['message' => 'Registro realizado com sucesso']);
        } else {
            echo json_encode(['error' => $validation]);
        }
    }

    public function edit(){
        $album = $this->albumService->getAlbumById($_GET['id']);

        echo json_encode($album);
    }

    public function update(){
        $validation = AlbumRequest::storeValidate($_GET);

        if (empty($validation)) {
            $headers = getallheaders();
            $token = substr($headers['Authorization'], 7);

            $partes = explode('.', $token);
            if (count($partes) === 3) {
                $payload = base64_decode($partes[1]);
                $payload = json_decode($payload, true);

                $userId = $payload['user_id'];
            } else {
                echo "Token JWT inválido!";
            }

            $this->albumService->updateAlbum($_GET['id'], $_GET['title'], $userId);

            echo json_encode(['message' => 'Registro atualizado com sucesso']);
        } else {
            echo json_encode(['error' => $validation]);
        }
    }

    public function delete(){
        $this->albumService->deleteAlbum($_GET['id']);

        echo json_encode(['message' => 'Registro deletado com sucesso']);
    }
}