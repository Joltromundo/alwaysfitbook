<?php
namespace app\Http\Controllers;

use app\Services\AuthService;

class AuthController {
    private $authService;

    public function __construct(AuthService $authService) {
        $this->authService = $authService;
    }

    public function auth(){
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $email = $_POST['email'];
            $password = $_POST['password'];

            $user = $this->authService->authenticate($email, $password);
           
            if ($user) {
                $payload = array(
                    "user_id" => $user['id'],
                    "exp" => time() + 3600 
                );
                
                $payload_json = json_encode($payload);
            
                $header = base64_encode(json_encode(array("alg" => "HS256", "typ" => "JWT")));
                $payload_base64 = base64_encode($payload_json);
                $signature = hash_hmac("sha256", "$header.$payload_base64", "d2YYDHyL9P3YGJ2q9jKVeySMhwpXj", true);
                $signature_base64 = base64_encode($signature);
                $token = "$header.$payload_base64.$signature_base64";
            
                echo json_encode(array("token" => $token));
            } else {
                echo 'Login ou senha incorretos!';
            }
        }
    }
}