<?php
namespace app\Http\Requests;

class AlbumRequest {
    public static function storeValidate($data) {
        $errors = [];

        if (empty($data['title'])) {
            $errors[] = 'O campo title é obrigatório.';
        }

        return $errors;
    }
}