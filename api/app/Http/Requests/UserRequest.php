<?php
namespace app\Http\Requests;

class UserRequest {
    public static function storeValidate($data) {
        $errors = [];

        if (empty($data['name'])) {
            $errors[] = 'O campo nome é obrigatório.';
        }

        if (empty($data['email']) || !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
            $errors[] = 'O campo email é inválido.';
        }

        if (empty($data['password']) || strlen($data['password']) < 6) {
            $errors[] = 'A senha deve ter no mínimo 6 caracteres.';
        }

        return $errors;
    }
}