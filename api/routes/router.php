<?php

use app\Http\Controllers\AlbumController;
use app\Http\Controllers\AuthController;
use app\Http\Controllers\PhotoController;
use app\Http\Controllers\RegisterController;
use app\Http\Middleware\JwtMiddleware;
use app\Services\AlbumService;
use app\Services\AuthService;
use app\Services\PhotoService;
use app\Services\UserService;

$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

switch($url){
    case '/':
        echo 'Deus te abençoe';
    break;

    case '/api/auth':
        $authService = new AuthService();
        $authController = new AuthController($authService);
        $authController->auth();
    break;

    case '/api/register':
        $userService = new UserService();
        $registerController = new RegisterController($userService);
        $registerController->store();
    break;

    case '/api/albums':
        $jwtMiddleware = new JwtMiddleware();
        $jwtMiddleware->handle($_SERVER['HTTP_AUTHORIZATION'], function ($request) {
            $albumService = new AlbumService();
            $albumController = new AlbumController($albumService);

            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    if (isset($_GET['id'])) {
                        $albumController->edit();
                    } else {
                        $albumController->index();
                    }
                break;
            
                case 'POST':
                    $albumController->store();
                break;
            
                case 'PUT':
                    $albumController->update();
                break;
            
                case 'DELETE':
                    $albumController->delete();
                break;
            
                default:
                    http_response_code(405);
                    
                    echo "Método de requisição não suportado";
                break;
            }
        });
    break;

    case '/api/photos':
        $jwtMiddleware = new JwtMiddleware();
        $jwtMiddleware->handle($_SERVER['HTTP_AUTHORIZATION'], function ($request) {
            $photoService = new PhotoService();
            $photoController = new PhotoController($photoService);

            switch ($_SERVER['REQUEST_METHOD']) {
                case 'GET':
                    if (isset($_GET['id'])) {
                        $photoController->edit();
                    } else {
                        $photoController->index();
                    }
                break;
            
                case 'POST':
                    $photoController->store();
                break;
            
                case 'PUT':
                    $photoController->update();
                break;
            
                case 'DELETE':
                    $photoController->delete();
                break;
            
                default:
                    http_response_code(405);
                    
                    echo "Método de requisição não suportado";
                break;
            }
        });
    break;

    default:
        echo $url . ' - 404 not found';
    break;
}
?>